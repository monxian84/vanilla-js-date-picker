{
    let weekdays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    let fullMonths = ['January', 'Febuary', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    let dayMonthYear = false;

    let d = new Date();
    var date_obj = {
        cur_month: d.getMonth(),
        cur_year : d.getFullYear(),
        month : d.getMonth(),
        year : d.getFullYear(),
        date : d.getDate(),
    }
    date_obj.days_in_current_month = days_in_month( date_obj.year,  date_obj.month, 1);
    date_obj.days_in_last_month = days_in_month( date_obj.year, date_obj.month, 0);
    date_obj.first_day_of_month = first_day_of_month(date_obj.year, date_obj.month);

    document.querySelectorAll('.calendar-container').forEach(item => {
        item.addEventListener('click', event => {
            if(event.target.dataset.info == 'available'){
                let input_value = item.parentNode.childNodes[4];
                let day = (event.target.innerText);
                input_value.value = date_obj.month+1 + '/'+ day  + '/'+ date_obj.year;
                close_all_calenders();
            }
        })
    });

    //Returns the number of days in the month
    function days_in_month(year, month, month_adj_array){
        //Date is this date func is based on 1-12 months not 0-11
        return new Date(year, month+month_adj_array, 0).getDate(); 
    }

    function first_day_of_month(year, month){
        let date = new Date(year, month, 1);
        date = date.toDateString();
        date = date.split(" ");
        return weekdays.indexOf(date[0]);
    }

    function open_calendar(year, month){

        let input_clicked = document.getElementById(event.target.attributes.id.value);

        //get the calendar div, remove children from last call and close any open calendars 
        let calendar_container = input_clicked.nextElementSibling;
        
        //remove any tables from last call
        if(calendar_container.lastElementChild.nodeName == 'TABLE'){
           calendar_container.removeChild(calendar_container.lastElementChild);
         }
        //close any open calendars
        close_all_calenders();

        //show the calendar
        calendar_container.classList.add('show');
       
        //add year and month to the calendar header
        let header_contents = calendar_container.childNodes[1].childNodes[3];
        header_contents.innerText = fullMonths[date_obj.month]+" "+ date_obj.year
        
        let table = build_table_of_days(date_obj.days_in_last_month, date_obj.days_in_current_month, date_obj.first_day_of_month);
      
     calendar_container.appendChild(table);

    }//end of open_calendar



function build_table_of_days(last_month_days, current_days, first_day){
    let table = document.createElement('table');

    //add the days of the week to the top
    let tableTr = document.createElement('tr')
    for(let i = 0; i < weekdays.length; i++){
        let tableTh = document.createElement('th');
        let thText = document.createTextNode(weekdays[i]);
        tableTh.appendChild(thText);
        tableTr.appendChild(tableTh);
    }
    
    
    table.appendChild(tableTr);

    //build the rows of days

    // are you looping through previous months days or looping through next months days
    let previous_days = true;
    let next_month_days = false;
    var offset, count;
   
    if(first_day > 0){
        offset = (last_month_days - first_day)  ;
       
    } else {
        previous_days = false;
        offset = 0;
        count = 1;
    }

  

    //loop throught weeks on calendar
    
    for(let i=0; i < 6 ; i++){
        let table_tr = document.createElement('tr');
               
        //loop through the days of the week
        for(let j = 0; j < 7; j++){
            var rowTd = document.createElement('td');
            
            //Is this previous month?
            if(i == 0 && offset >= last_month_days) {
                previous_days = false;
                offset = 0;
                count = 1;
            } 
            
            //Is this next month?
            if(i >= 4 && count > current_days && next_month_days == false){
                next_month_days = true;
                offset = 0;
            }
            
            //Only allow days in current month
            if(previous_days == true || next_month_days == true ){
               offset++; 
               rowTd.className = 'unavailable-days';
               rowTd.setAttribute('data-info', 'n/a');
               count = offset;
            } else {
                rowTd.className = 'available-days';
                rowTd.setAttribute('data-info', 'available');
            }

            var tdText = document.createTextNode(count);
            if(date_obj.date == count && i > 1 && date_obj.month == date_obj.cur_month && date_obj.year == date_obj.cur_year ){
                rowTd.className = 'current-day';
                rowTd.appendChild(tdText);
            } else {
                rowTd.appendChild(tdText);
            }
            count++;
            //rowTd.onclick = pickedDayEvent; // Attach the event!
            table_tr.appendChild(rowTd);
        }
        
        table.appendChild(table_tr);
    }

    return table;
}

//previous and next month functions
function previous_month(){
    let table_parent = event.target.parentNode.parentNode;
    let table = event.target.parentNode.nextElementSibling;
   
    //if month is January go to December previous year
    if(date_obj.month == 0){
        date_obj.month = 11;
        date_obj.year = date_obj.year - 1;
    } else {
        date_obj.month--;
    }
    let header_content =  event.target.parentNode.childNodes[3];
    header_content.innerHTML = fullMonths[date_obj.month] + " " + date_obj.year;

    get_days_info(date_obj.year, date_obj.month);
    let new_table = build_table_of_days(date_obj.days_in_last_month, date_obj.days_in_current_month, date_obj.first_day_of_month);
    table_parent.replaceChild(new_table, table)
}

function next_month(){
    let table_parent = event.target.parentNode.parentNode;
    let table = event.target.parentNode.nextElementSibling;
    
    //if month is December go to January next year
    if(date_obj.month == 11){
        date_obj.month = 0;
        date_obj.year = date_obj.year + 1;
    } else {
        date_obj.month++;
    }

    let header_content =  event.target.parentNode.childNodes[3];
    header_content.innerHTML = fullMonths[date_obj.month] + " " + date_obj.year;

    get_days_info(date_obj.year, date_obj.month);
    let new_table = build_table_of_days(date_obj.days_in_last_month, date_obj.days_in_current_month, date_obj.first_day_of_month);
    
  table_parent.replaceChild(new_table, table)
}

function get_days_info(year, month){
    date_obj.days_in_current_month = days_in_month( year,  month, 1);
    date_obj.days_in_last_month = days_in_month( year, month, 0);
    date_obj.first_day_of_month = first_day_of_month(year, month);
}

function close_all_calenders(){
    let close_calendar = document.getElementsByClassName('calendar-container');
        for(let i = 0; i < close_calendar.length; i++){
            close_calendar[i].classList.remove('show');
    }
}

 // When the user clicks anywhere outside of the modal, close it
 window.onclick = function(event) {
    event.stopPropagation();

    let tag = event.target.tagName;
    
   if (tag == "HTML" || tag == "BODY") {
        //close any open calendars
        close_all_calenders();

   } 
 } 





}


